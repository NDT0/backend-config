# Template for setting up the environment and CI/CD (Server web application)

Template for setting up DEV, STAGING, RELEASE environments and CI/CD. This template was written for django applications 
with data storage in a postgresql database


## Getting started

1. Create an ssh key using the command:

    ```ssh-keygen -t ed25519 -C "Gitlab SSH key"```

    **Important!** Do not enter a passphrase. Skip the action by pressing Enter

2. Put the private part of the key in the gitlab variable (Settings - CI/CD - Variables) with the name
   `SSH_PRIVATE_KEY`, and the public one - to your dedicated server (in autorized_keys file).
3. Create a gitlab variable named `SERVER_IPADDRESS` and put the IP address of the dedicated server there.
4. Copy the files `.gitlab-ci.yml, docker-compose.*.yaml` and place them in the root directory of your application.
5. Put the Dockerfile in the source directory (./src) of your project
6. Make sure that in the CI/CD .gitlab-ci.yml configuration file, the branch names correspond to the branches of your project.
    ```
   deploy-staging:
    stage: deploy
    only:
    - staging
   ```
   The only parameter is responsible for the name of the branch to which the trigger on the branch merge is added
7. Edit `docker-compose.*.yaml` files according to the needs of your project
   
   **Important!** Observe the required port numbers for different environments. NGINX of the dedicated server listens on these ports

   | Environment  | Ports          |
   |--------------| ---------------|
   | LOCAL        | any            |
   | DEV          | 8000           |
   | STAGING      | 8001           |
   | RELEASE      | 8002           |

   It is a good practice to store variable credentials in gitlab variables and pass them to docker-compose when CI/CD is executed.
